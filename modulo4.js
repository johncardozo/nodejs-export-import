class Usuario {
  constructor(nombre, edad, email) {
    this.nombre = nombre;
    this.edad = edad;
    this.email = email;
  }

  getDatosUsuario() {
    return `
      nombre: ${this.nombre}
      edad: ${this.edad}
      Email: ${this.email}
    `;
  }
}

module.exports = Usuario;
