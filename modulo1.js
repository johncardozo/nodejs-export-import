const obtenerNombre = () => {
  return "John";
};

const obtenerCiudad = () => {
  return "Medellin";
};

const fechaNacimiento = "1518-08-30";

exports.obtenerNombre = obtenerNombre;
exports.obtenerCiudad = obtenerCiudad;
exports.fechaNacimiento = fechaNacimiento;
