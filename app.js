// Importar desde módulo
const usuario1 = require("./modulo1");
const usuario2 = require("./modulo2");
// Importar desde un módulo desestructurando
const { obtenerPais, obtenerDepartamentos } = require("./modulo3");
// Importar desde un módulo que exporta con module.exports
const Usuario = require("./modulo4");
// Importar desde un módulo que exporta con module.exports con sintaxis alterna
const { getEquipo, fecha } = require("./modulo5");

// DATOS DESDE MODULO 1
console.log(`Nombre: ${usuario1.obtenerNombre()}`);
console.log(`Ciudad: ${usuario1.obtenerCiudad()}`);
console.log(`Fecha de Nacimiento: ${usuario1.fechaNacimiento}`);

// DATOS DESDE MODULO 2
console.log(`Usuario: ${usuario2.obtenerUsuario()}`);
console.log(`Edad: ${usuario2.obtenerEdad()}`);
console.log(`RH: ${usuario2.rh}`);

// DATOS DESDE MODULO 3
console.log(`Pais: ${obtenerPais()}`);
console.log(`Departamento: ${obtenerDepartamentos()}`);

// DATOS DESDE MODULO 4
const usuario3 = new Usuario("John", 40, "johncardozo@gmail.com");
console.log(usuario3.getDatosUsuario());

// DATOS DESDE MODULO 5
console.log(`${getEquipo()} nació en ${fecha}.`);
